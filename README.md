Fedora 33 - Package Repository
==============================

Repository Setup
----------------

Install with dnf repository.

```
% sudo tee -a /etc/yum.repos.d/kjdev.repo <<EOM
[kjdev]
name=Fedora \$releasever - kjdev
baseurl=https://kjdev.gitlab.io/fc$releasever.pkg/rpm/
enabled=1
gpgcheck=0

[kjdev-debuginfo]
name=Fedora \$releasever - kjdev - Debug
baseurl=https://kjdev.gitlab.io/fc$releasever.pkg/debug/
enabled=0
gpgcheck=0

[kjdev-source]
name=Fedora \$releasever - kjdev - Source
baseurl=https://kjdev.gitlab.io/fc$releasever.pkg/source/
enabled=0
gpgcheck=0
EOM
```

RPM Install
-----------

```
% dnf install <PACKAGE>
```
